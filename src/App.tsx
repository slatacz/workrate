import {useState,useEffect} from 'react';
import './App.css';

interface ImageObject{
  id: string,
  author: string,
  width: number,
  height: number,
  url: string,
  download_url: string
}

function App() {
  const [urls,setUrls] = useState<Array<string> | null>(null)
  const [pagination, setPagination] = useState<number>(0)
  const [animation, setAnimation] = useState<boolean>(true)
  const numberOfImages = 3;

  useEffect(()=>{
    fetch('https://picsum.photos/v2/list')
      .then((response)=>response.json())
      .then((data:Array<ImageObject>)=> {
        const strings:Array<string> = data.map((element:ImageObject)=>{
          const url = element.url;
          return `http://source.unsplash.com/${url.substring(url.lastIndexOf('/') + 1)}`
        })
        setUrls(strings)
      })
  },[])

  const next = ()=>{
    setAnimation(true)
    setPagination(prev=>prev+1)
  }

  const previous = ()=>{
    setAnimation(false)
    setPagination(prev=>prev-1)
  }

  return (
    <div className="App">
      {urls===null? <div>Loading ...</div>
        :<div className={animation?'next':'previous'} key={pagination}>
          <div>
            <img src={urls[pagination*numberOfImages]} alt='' />
            <img src={urls[pagination*numberOfImages+1]} alt='' />
            <img src={urls[pagination*numberOfImages+2]} alt='' />
          </div>
          <div className='controls'>
            {pagination > 0 && <button onClick={previous}>Previous</button>}
            {pagination*numberOfImages+numberOfImages < urls.length && <button onClick={next}>Next</button>} 
          </div>
        </div>
      }
    </div>
  );
}

export default App;
